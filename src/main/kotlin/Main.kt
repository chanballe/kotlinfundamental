fun main() {
    //example of list collections
        // immutable - cannot be changed nor add/delete data
//    val airlines = listOf("Cebu Pacific", "PAL", "Airasia")
////    println(airlines)
////    println(airlines[1])
////    println(airlines.get(1))
//
//        // mutable - can be changed, add/dlete data
//    val terminals = mutableListOf<Int>(1, 2, 3, 4)
//    println(terminals)
//    terminals.add(5)
//    println(terminals)
//    println(terminals.contains(2))

//    // immutable
//    val myHobbies: MutableList<String>  = mutableListOf("Planting", "Cooking", "Woodworking", "Running", "Reading")
//    println(myHobbies)
//    println(myHobbies.add("Biking")) //recent data of variable myHobbies
//    println(myHobbies)
//    println(myHobbies.remove("Cooking")) //boolean output
//    println(myHobbies) //recent data of variable myHobbies
//
//    //mutable sample
//    val myData = listOf("Sinovac", "Pfizer", "Moderna", "Sputnik", "AstraZenica", "Janssen")
//    println(myData)
//    println(myData.size)
//    println(myData.lastIndexOf("Sputnik"))
//    println(myData.first())

    // set is immutable
//    val usernames: Set<String> = setOf("brandon", "amelia", "charlie")
//    val myHobbiesSet: Set<String>  = setOf("Planting", "Cooking", "Woodworking", "Running", "Reading")
//    println(myHobbiesSet)
//    println(myHobbiesSet.size)     //Count of usernames
//    println(myHobbiesSet.equals("Biking")) //what's the difference with .contains
//    println(myHobbiesSet.drop(2)) // drop index 0 to 1
//
//    //Set is mutable
//    val students: MutableSet<String> = mutableSetOf("Brandon", "Amelia", "Charlie")
//    val myDataSet: MutableSet<String> = mutableSetOf("Sinovac", "Pfizer", "Moderna", "Sputnik", "AstraZenica")
//    println(myDataSet)
//    myDataSet.add("Janssen")
//    println(myDataSet)
//    myDataSet.add("pfizer") //case-sensitive
//    println(myDataSet)

    // MAP -> key-value pairs
    //immutable
//    val studentDetails: Map<String, String> = mapOf(
//        "firstName" to "Brandon",
//        "lastName" to "Diaz",
//        "yrLevel" to "first",
//        "section" to "A"
//    )
//    println(studentDetails)         //inside iterations
//    println(studentDetails.keys)        //getting the keys
//    println(studentDetails.values)      //getting the values

    //mutable
//    val studentDetailsMutable: MutableMap<String, String> = mutableMapOf(
//        "firstName" to "Brandon",
//        "lastName" to "Diaz",
//        "yrLevel" to "first",
//        "section" to "A"
//    )
//    println(studentDetailsMutable)         //inside iterations
//    studentDetailsMutable.remove("section")     //removing entire key "section"
//    println(studentDetailsMutable)

    val myProduct: MutableMap<String, String> = mutableMapOf(
        "itemName" to "TV Rack",
        "Type" to "Furniture",
        "Status" to "Active",
        "Delivery Location" to "Manila",
        "Contact Person" to "Juan Dela Cruz"
    )
//    println(myProduct)
//    myProduct.put("Condition", "Used")  // adding key and value
//    myProduct["Condition"] = "Used"     //same output for this syntax myProduct.put("Condition", "Used")
//    println(myProduct)
//    myProduct.replace("Type", "Furniture", "Baby Stroller") //replacing the value of Type
//    println(myProduct)
//    print(myProduct.entries)    //showing all the entries
//    println(myProduct.containsKey("Condition"))     //showing if map has a key that contains "Condition" -> Boolean output
//    println(myProduct.size)     //showing the size of map
//    println(myProduct.computeIfAbsent("Status", ))


}