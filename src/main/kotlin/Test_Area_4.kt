//creating a function that has parameter
fun sayHello(userToGreet: String) {
    val msg = "Magandang Umaga $userToGreet"
    println(msg)
}

//creating single function parameter expression
fun sayHelloA(itemToGreet: String) = "Hello $itemToGreet"

//creating function with greater than one parameter expression
fun sayHelloB(greetings: String, myName: String) = println("$greetings $myName")

fun main() {
    //function sayHello with parameter
    sayHello(userToGreet = "Chan")
    sayHello(userToGreet = "Kotlin")

    //single function sayHelloA with parameter
    println(sayHelloA(itemToGreet = "Chan"))
    println(sayHelloA(itemToGreet = "Kotlin"))

    //function with two parameter
    sayHelloB(greetings = "Hi", myName = "Chan")

    //creating array of string
    val myHobbies = arrayOf("Planting", "Reading", "Woodworking", "Cooking", "Biking", "Running" )
    println(myHobbies[0])
    //can also use .get()
    println(myHobbies.get(3))

    //for-loop to show all the items in array
    for (myHobby in myHobbies){
        println(myHobby)
    }

    //displaying items in array using forEach
    myHobbies.forEach { myHobbyA ->
        println(myHobbyA)
    }
}



