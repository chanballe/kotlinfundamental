fun main(){

    //declaration of variable
    val nameDirectory = mutableListOf<String>()
    val birthdayDirectory = mutableListOf<Any>()
    var i = 0

    //it will run if this condition is satisfied (i < 10) && (name!="ZZZ")
    do{
        print("Enter a name: ")
        val name = readLine()!!

        //to see if the input name is already in the nameDirectory
        if (name in nameDirectory){
            val indexName = nameDirectory.indexOf(name)
            println("Found on the list, $name's birthday is on " + birthdayDirectory[indexName] + "\n")
        } else if ( name == "ZZZ" ) {           //it will only break the loop if ZZZ was entered
            break
        } else{                                 //this condition will satisfy, if the input name was not in the directory
            nameDirectory += name
            print("Enter $name's birthday: ")
            val birthday = readLine()!!
            birthdayDirectory += birthday

        }

        i += 1      //to increment the loop until given while condition

    } while ((i < 10) && (name!="ZZZ"))

    println("Done entering 10 new names or Invalid input ZZZ")

}
