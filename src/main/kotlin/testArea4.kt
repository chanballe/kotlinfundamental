fun main(args: Array<String>){

    val pokemon1 = Pokemon("Bulbasaur", "Grass")
    println("New pokemon encountered: " + pokemon1.name)
    println("Pokemon type: " + pokemon1.type)

    val pokemon2 = Pokemon("Charmander","Fire")
    println("New pokemon encountered: " + pokemon2.name)
    println("Pokemon type: " + pokemon2.type)

//    pokemon2.tackle()

    startBattle(pokemon1 , pokemon2)
}


fun startBattle(challenger: Pokemon, champion: Pokemon) {

    while (!challenger.hasFainted() || !champion.hasFainted()) {

        if (champion.healthPoints > 0) {
            var damage = challenger.tackle()
            champion.healthPoints -= damage
            println(champion.name + "'s health is " + champion.healthPoints)
            if (champion.hasFainted()) {
                println(champion.name + " Lose. " + challenger.name + " Wins.")
                break
            } else if (challenger.healthPoints > 0) {
                damage = champion.tackle()
                challenger.healthPoints -= damage
                println(challenger.name + "'s health is " + challenger.healthPoints)
                    if (challenger.hasFainted()) {
                    println(challenger.name + " Lose. " + champion.name + " Wins")
                    break
                    }
            }
        }

    }
}