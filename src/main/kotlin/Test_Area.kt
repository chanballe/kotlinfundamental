import kotlin.math.absoluteValue

fun main() {

    print("Enter Father's age: ")
    val currentFatherAge = readLine()!!.toInt()

    print("Enter Son's age: ")
    val currentSonAge = readLine()!!.toInt()

    val result = 2 * currentSonAge - currentFatherAge
    print("The year that father's age is twice of son is ${result.absoluteValue} ago")

}