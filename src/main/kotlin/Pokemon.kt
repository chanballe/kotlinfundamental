class Pokemon( val name: String, val type: String) {
//    var name = name           ipapasok ito sa taas, constructor ang tawag doon
//    var type = type           ipapasok ito sa taas, constructor ang tawag doon
    var healthPoints = 100
    var attackPoints = 10

    fun tackle() : Int {
        println("$name attacked with $attackPoints dmg")
        return attackPoints
    }

    fun hasFainted(): Boolean {
        return healthPoints <= 0
    }
}

