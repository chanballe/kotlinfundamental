import java.util.*
import kotlin.collections.ArrayList

fun main(){
    //Activity 1
//    print("Enter first number: ")
//    val firstNumber = readLine()!!.toInt()
//    print("enter second number: ")
//    val secondNumber = readLine()!!.toInt()
//    print("enter third number: ")
//    val thirdNumber = readLine()!!.toInt()
//
//    when {
//        (firstNumber>secondNumber) && (firstNumber>thirdNumber) -> println("the greatest number is $firstNumber")
//        (secondNumber>firstNumber) && (secondNumber>thirdNumber) -> println("the greatest number is $secondNumber")
//        (thirdNumber>secondNumber) && (thirdNumber>firstNumber) -> println("the greatest number is $thirdNumber")
//        else -> println("Invalid input")
//    }
    //Activity 2
//    print("Enter a letter: ")
//    val inputLetter: Char = readLine()!!.lowercase().toCharArray()[0]
//
//    val consonantVowel = if (inputLetter == 'a' || inputLetter == 'e' || inputLetter == 'i' || inputLetter == 'o' || inputLetter == 'u') "vowel" else "consonant"
//
//    println("Letter $inputLetter is $consonantVowel")

    // Activity 3
    print("Enter your password: ")
    val password = readLine()!!
    val passwordSize = password.length
    val characters: List<String> = listOf("$", "#", "@")
    val numbers = listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).toString()
    var lowerCase = false
    var upperCase = false
    var character = false
    var number = false
    var otherCharacter = true

    if ((passwordSize >= 6) &&  (passwordSize <= 16)){
        for(char in password){
            when (char.toString()) {
                in "a".."z" -> lowerCase = true
                in "A".."Z" -> upperCase = true
                in characters -> character = true
                in numbers -> number = true
                else -> otherCharacter = false
            }
        }
        if (lowerCase && upperCase && character && number && otherCharacter) {
            println("Valid Password")
        } else {
            println("Invalid Password")
        }
    } else {
        println("Invalid password")
    }
}


