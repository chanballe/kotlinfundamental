fun main(){
    val numbers = listOf(1,2,3,4,5)

    //forEach
    numbers.forEach {
        val newNumber = it * 15
        println(newNumber)
    }

    //map -> will return a NEW collection, performing the transformation
    val newNumbers = numbers.map{
        it * 4              //this will perform but will not return the output
        it * 3              //this will perform but will not return the output
        it * 2              //this will perform and will return the output, always the last operation will be performed
    }
    println(newNumbers)

    //filter -> will return a new collection, filtering the values based on our condition

    val evenNumbers = numbers.filter {
        it % 2 == 0
    }
    println(evenNumbers)

    //function
    val number = 4
    println(isEven(number))

    val number2 = 3
    println(isEven(number2))
}


//function to check whether a number is odd or even
fun isEven(num: Int): Boolean {
    return if( num % 2 == 0 ) true
    else false
}