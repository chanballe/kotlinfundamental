var name: String = "Chan"
var greeting: String? = "Hello"
var greeting2: String = "Hi"
var greeting3: String? = "Ohayou"
var greeting4: String? = "Magandang Umaga"

//creating another function (does this like a def class in python?
// I don't know yet)
fun getGreeting1(): String {
    return "Magandang Umaga Chan, let's learn Kotlin"
}

//creating single function expression
fun getGreetingA(): String = "Hello Kotlin"
//creating single function expression to this
fun getGreetingB() = "Hi Kotlin"

fun main(){

    print(greeting)
    println(" $name")

    //putting null in variable put "?" on string type to variable declaration
    greeting = null
    println(greeting)

    println("$greeting2 $name")
    //another way of putting null in variable, put .toString() to the variable even without putting "?"
    // to variable declaration
    greeting2 = null.toString()
    println(greeting2)

    //control flow (if-else) stating that greeting3 is not null
    if(greeting3 != null) {
        println("greeting is not null")
        } else {
            println(greeting3)
            }
    //control flow (when) stating that greeting3 is null
    greeting3 = null
    when(greeting3) {
        null -> println("greeting is null")
        else -> println("greeting is not null")
    }

    //if-else as an expression, it will show "Magandang Umaga" example below
    val greetingToPrint = if(greeting4 != null) greeting4 else "Magandang Hapon"
    println(greetingToPrint)

    //when as an expression, it will show "Magandang Hapon"
    greeting4 = null
    val greetingToPrint2 = when (greeting4) {
        null -> "Magandang Gabi"
        else -> "Magandang Hapon"
    }
    println(greetingToPrint2)

    //calling and show getGreeting1 function
    println(getGreeting1())

    //calling and show getGreetingA function
    println(getGreetingA())
    println(getGreetingB())
}