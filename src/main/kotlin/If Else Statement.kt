fun main(args: Array<String>) {
    print("Hello What's your name: ")
    var name = readLine()!!

    //Ask for user's age
    print("$name, how old are you? ")
    var age = readLine()!!.toInt()

    // if user is underage, say that user is not allowed to use the service
    if (age >= 0) {
        if(age == 17) {
            println("you need a guardian!")
        } else if (age < 18) {
            println("you are not allowed to use the service!")
        } else {
            println("Welcome to NBI")
        }
    }
}