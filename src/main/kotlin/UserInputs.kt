import java.time.LocalDate

fun main () {
    print("Enter your name: ")
    val myName = readLine()
    print("Enter your birth month: ")
    val myMonth = readLine()?.toInt()
    print("Enter your birth day: ")
    val myDay = readLine()?.toInt()
    print("Enter your birth year: ")
    val myYear = readLine()?.toInt()
    println("Your birthday is $myMonth $myDay $myYear" )

    val currentDate = LocalDate.now()
    val currentMonth = currentDate.monthValue
    val currentDay = currentDate.dayOfMonth
    val currentYear = currentDate.year

    var ageMonth = currentMonth - myMonth!!
    var ageDay = currentDay - myDay!!
    val ageYear = currentYear - myYear!!

    if (ageDay < 0) {
        ageMonth -= 1
        ageDay += 30
    }

    println("$myName, your age is $ageMonth months $ageDay days $ageYear years")
}

