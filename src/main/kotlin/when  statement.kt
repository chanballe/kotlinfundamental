fun main(){
//    println("What is the day today?")
//    val dayOfWeek: String = readLine()!!.lowercase()
//
//    when (dayOfWeek) {
//        "monday" -> println("Hey it's Monday! Let's be productive")
//        "tuesday" -> println("Hey it's Tuesday! Choose to be happy!")
//        "wednesday" -> println("Hey it's Wednesday! my dudes!")
//        "thursday" -> println("Hey it's Thursday! Let's be productive")
//        "friday" -> println("Thank God it's friday")
//        else -> println("It's not me, it's you!")
//    }

    //**Calculator**
    // Ask for first number
    print("Enter first number: ")
    val firstNumber = readLine()!!.toDouble()

    // Ask for second number
    print("Enter second number:")
    val secondNumber = readLine()!!.toDouble()

    // Ask for operation to be done
    print("Choose operator ( +, -, *, / ): ")
    val operator = readLine()!!

    // Perform the operation
    when (operator) {
        "+" -> println(firstNumber + secondNumber)
        "-" -> println(firstNumber - secondNumber)
        "*" -> println(firstNumber * secondNumber)
        "/" -> println(firstNumber / secondNumber)
        else -> println("Operator is not supported")
    }

    // Display result
}