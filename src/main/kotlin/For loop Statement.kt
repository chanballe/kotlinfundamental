fun main(args: Array<String>){
//    for (item in 1..5) {
//        println(item)
//    }

//    val fruits = mapOf<Int, String>(1 to "Banana", 2 to "Apple", 3 to "Orange")
////    println(fruits[1])    //this will print value in key 1
//
//    //will iterate all items in fruits
//    for ((key, value) in fruits) {          //can also use for ((key, value) in fruits) {, so you can omit .value command
//        println(key)       //to show the value of items in fruits, using .value
//    }

//    val users = arrayListOf<String>("Ferdie", "Lisa", "Mary")
//    val ages = arrayListOf<Int>(45,13, 24)
//
//    for (count in 0 until users.lastIndex) {
//        val username = users[count]
//        val age = ages[count]
//
//        println("$username is $age years old")
//    }

    var exitCode: Int = -1
        //To perform increment operatioon until we encounter exit code


    // Human counter

    var totalHumans = 0
    var userInput = 0

    do {
        println("How many people entered: ")
        userInput = readLine()!!.toInt()
            if(userInput != -1) totalHumans += userInput
            if(userInput != -1) println("*****$totalHumans*****")
//        println("How many people entered: ")
//        userInput = readLine()!!.toInt()
    } while(userInput != exitCode)

    println("Thank you for using HumanCounter!")
}