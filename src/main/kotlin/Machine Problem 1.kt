fun main() {
    print("Enter a letter not more than 15: ")
    val inputCharacters: String = readLine()!!
    val inputList = inputCharacters.toCharArray()

    //to check if the character consist of letters only
    when (isCharacter(inputCharacters)) {
        true -> if (isLength(inputCharacters) > 0) {
                    if(isLength(inputCharacters) % 2 == 0) {
                        println(inputCharacters.reversed())
                    } else {
                        println(inputList.sorted().joinToString(""))
                    }
                } else {
                    println("Invalid Input")
                }
        else -> println("Invalid input")
    }

}

//function to return boolean if the input characters consist of letters only
fun isCharacter(characters: String): Boolean {
    for (character in characters)
    {
        if (character !in 'A'..'Z' && character !in 'a'..'z') {
            return false
        }
    }
    return true
}

/**
 * Returns a modifies string depending on the condition
 * If even, reversed
 * If add, in alphabetical order
 */

//function to return the length of the input character
fun isLength(characters: String): Int {
    val lengthCharacter = characters.length
    return if(lengthCharacter in 0..15){
        lengthCharacter
    } else {
        0
    }
}

