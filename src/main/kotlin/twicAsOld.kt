//import Kata.digitize

//import ASum.findNb
//import kotlin.math.roundToInt

//code-wars twiceasold
//fun twiceAsOld(dadYearsOld: Int, sonYearsOld: Int): Int {
//    return 2 * sonYearsOld - dadYearsOld
//}
//
//fun main(){
//    println(twiceAsOld(dadYearsOld = 35, sonYearsOld = 4))
//}

//code-wars Century
//fun century(number: Int): Int {
//    if (number < 0 ){
//        println("year is invalid")
//    } else if (number < 100){
//        println("1 century")
//    } else if (number % 100 == 0){
//        val year: Int = number / 100
//        println("$year century")
//    } else {
//        val yearA: Int = number / 100 + 1
//        println("$yearA century")
//    }
//    return number
//}
//
//fun main(){
//    century(number= 1705)
//}

//code-war Opposites Attract
//fun loveFun(flowerA: Int, flowerB: Int): Boolean {
//    return (flowerA % 2 == 0) != (flowerB % 2 == 0)
//}
//
//fun main(){
////    var x = intArrayOf(1, 3, 2, 5, 3)
////    var y: = longArrayOf(x)
//    print(isLeapYear(year = 2000))
//}

//code war covert number to reversed array of digits
//object Kata {
//    fun digitize(n:Long):IntArray {
//        // convert Long to string then reversed, then getting numeric value of string, then convert to IntArray
//        return n.toString().reversed().map { x -> Character.getNumericValue(x) }.toIntArray()
//    }
//}

//code war alphabet
//fun alphabetWar(fight: String): String {
//    val leftLetters = "sbpw"
//    val rightLetters = "zdqm"
//    var leftScore = 0
//    var rightScore  = 0
//    // getting the score of left and right letters
//    for (i in fight){
//        if(leftLetters.contains(i)){
//            leftScore+=leftLetters.indexOf(i) + 1
//        }
//        if(rightLetters.contains(i)){
//            rightScore+=rightLetters.indexOf(i) + 1
//        }
//    }
//    //result whether left or right letters win
//    return if(leftScore>rightScore){
//        "Left side wins!"
//    } else if(leftScore<rightScore){
//        "Right side wins!"
//    } else {
//        "Let's fight again!"
//    }
//}

//code-war leap year
//fun isLeapYear(year: Int) : Boolean {
//    var leap = false
//    return if ((year % 4) == 0) {
//        if ((year % 100) ==0){
//            leap = (year % 400) == 0
//            leap
//        } else {
//            true
//        }
//    } else {
//        false
//    }
//}



//code-war Maximum Multiple
//fun maxMultiple(d: Int, b: Int): Int {
//    return if (b % d == 0){
//        b
//    } else {
//        b - (b % d)
//    }
//}

//code-war convert number to reversed array digit
//object Kata {
//    fun digitize(n: Long): List<Int> {
//        val toStringN = n.toString()
////        return toStringN
//        val reverseN = toStringN.reversed()
////        return reverseN
//        val splitReverseN = reverseN.split("")
////        return splitReverseN
//        return splitReverseN.map { it.toInt() }
////        return reverseN.map { it.code }
//    }
//}


//code-war Is n divisible by x and y?
//fun isDivisible(n: Int, x: Int, y: Int): Boolean {
//    return ( n % x == 0) && (n % y == 0)
//}


//code-war int to doubleint
//package solution
//
//fun doubleInteger(i:Int):Int {
//    // Double the integer and return it!
//    return i * 2
//}

//code-wars invert values
//fun invert(arr: IntArray): IntArray {
//    var arrayIndex = 0
//    //while loop to multiply every value in indexes by -1
//    while (arrayIndex < arr.size) {
//        arr[arrayIndex] = arr[arrayIndex] * -1
//        arrayIndex++
//    }
//    return arr
//}

//code-war keep hydrated!
//fun litres(time: Double): Int {
//    val rate = 0.5 * time
//    return rate.toInt()
//}

//code-war make negative
//class Kata {
//
//    fun makeNegative(x: Int): Int {
//        return -Math.abs(x)
//    }
//}

//code-war remove first and last character
//fun removeChar(str: String): String {
//    return str.substring(1,str.lastIndex)
//}

//code-war century from a year
//fun century(number: Int): Int {
//    return if(number % 100 == 0) ceil((number.toInt() / 100 - 1).toDouble()).toInt() + 1 else ceil((number.toInt() / 100).toDouble()).toInt() + 1
//}

//code-war Build a pile
//object ASum {
//
//    fun findNb(m: Long): Long {
//        var sum: Long = 0
//        var n: Long = 0
//        for (n in 1..m) sum += n * n * n
//        return if(sum == m) n-1 else -1
//
//    }
//}

//code-war Build a pile option2
//object ASum {
//
//    fun findNb(m: Long): Long {
//    var n: Long = 0
//    var sum: Long = 0
//        //while sum is less than the total given volume
//    while (sum < m) {
//        sum += n * n * n
//        n++
//    }
//        //once sum is equal to total given volume, n-1 or else -1
//    return if (sum == m) n - 1 else -1
//    }
//}
