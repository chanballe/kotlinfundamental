fun main(){
//    val message = "Welcome to Kotlin Tutorials"
//    message.toInt()

    try {
        val message = 3
        message.toInt()
    } catch (exception: NumberFormatException){
        println(exception.message)
    } finally {
        println("with errors")
    }
}