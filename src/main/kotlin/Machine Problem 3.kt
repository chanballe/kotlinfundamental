fun main() {

    val directory = mutableMapOf<String, String>(
        "anna" to "43662",
        "bret" to "46374",
        "chan" to "58796",
        "denver" to "85964",
        "eli" to "300045",
        "francis" to "47385",
        "gemma" to "45674",
        "henry" to "65748",
        "isaac" to "68593",
        "mateo" to "88432"
    )

    var quit = false

    //while variable quit satisfy the condition !quit, it will loop the Map directory to 30 times via its map size
    while(!quit){

        //if director size is less than 30, proceed to asking the name
        if(directory.size <= 30) {
            print("Please enter a name: ")
            val userName = readLine()!!.lowercase()         //.lowercase is used for converting the string to lowercase

            //this will check if the user typed the string "quit", if not it will run the function isDirectory
            if (userName != "quit") {
                isDirectory(directory, userName)
            }
            else {
                quit = true
            }
        }else {
            println("Full Directory!")
        }
    }

}

fun isDirectory(directoryList: MutableMap<String,String>, myName: String){

    //if the userName that have been entered which contains in map "directory", it will give the number
    if (directoryList.containsKey(myName)) {
        println("Number: ${directoryList.getValue(myName)} \n")
    } else {      //if userName was not found in map "directory", it will ask the number and store it in map "directory
        print("Please enter a phone number: ")
        val myNumber = readLine()!!
        directoryList[myName] = myNumber
        println("Contact $myName with a number of $myNumber has been added to the directory.\n")
    }
}