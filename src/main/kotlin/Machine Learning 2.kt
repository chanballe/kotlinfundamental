fun main() {

    val inputs = listOf("red", "blue", "Blue", "pink", "red", "yeLLow", "Indigo", "GrEEn")
    println(isGroupBy(inputs))

}

/**
 * return the function that will group according to the color in ROYGBIV
 * If it is in rainbowColors, it will remain it's string and then count according to how many times it appears
 * else it will group it to others and count in others
 */

fun isGroupBy(sameColor: List<String>): Map<String, Int> {
    val rainbowColors = listOf("red", "orange", "yellow", "green", "blue", "indigo", "violet")

    //will convert all letters to lower case
    val newInputs = mutableListOf<String>()
    sameColor.forEach {
        newInputs += it.lowercase()
    }

    //will convert letters from newInputs variable that contains in rainbowColors and
    // if not, will replace that string to others
    val newInputsChange = mutableListOf<String>()
    newInputs.forEach {
        newInputsChange += if (it !in rainbowColors) {
            it.replace(it, "others")
        } else {
            it
        }
    }

    //will group and count according to their string and return its new set of Map
    return newInputsChange.groupingBy { it }.eachCount().filter { it.value >= 1 }
}
