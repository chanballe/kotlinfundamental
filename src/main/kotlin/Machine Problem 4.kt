fun main(){
    val arrayQuestions = arrayListOf<String>(
        "Who is the first woman president in the Philippines? \nA.Gloria Macapagal Arroyo \nB.Cory Aquino \nC. Miriam Santiago",
        "Who is the  13th Philippine President? \nA.Joseph Estrada \nB.Noynoy Aquino \nC.Emilio Aguinaldo",
        "How long is the term of former President Erap Estrada? \nA.1.5 years \nB.6 years \nC.3 years",
        "Who is the first Philippine President? \nA.Ferdinand Marcos \nB.Diosdado Macapagal \nC.Emilio Aguinaldo",
        "How many branches of Philippine government? \nA.3 \nB.5 \nC.6",
        "What year is the EDSA 1 Revolution? \nA.1986 \nB.2004 \nC.2001",
        "How many island does the Philippines have in 2021? \nA.7601 \nB.7701 \nC.7107",
        "How long is the term  of a president in the Philippines? \nA.6 \nB.5 \nC.10",
        "Number of year per term in the House of Representative in the Philippines? \nA.3 \nB.6 \nC.4",
        "What year magellan discovered the Philippines? \nA.1534 \nB.1621 \nC.1521"
    )

    val correctAnswers = arrayListOf<String>( "B", "A", "A", "C", "A", "A", "C", "A", "A", "C" )

    println(isQuestionAndAnswer(arrayQuestions,correctAnswers))

}

fun isQuestionAndAnswer(arrayQuestion: ArrayList<String>, correctAnswer: ArrayList<String>): Map<String, Int> {

    //variable declaration
    var i = 0
    val result = mutableListOf<String>()

    println("To begin, you should enter your answers A, B or C. \nOtherwise, your input will be invalid. Good luck!\n")

    //will do the function until 9 or less than 10
    do{
        val number = i + 1
        println( "$number. " + arrayQuestion[i] )
        val userAnswer: String = readLine()!!

        //will check if the input is String only
        if( !( "[0-9D-Zd-z!#$@&^%()]".toRegex().matches( userAnswer ) ) ){

            val userAnswerUpperCase = userAnswer.uppercase()      //this will convert the input string to upper case

            //this will check if the user input is correct
            if( userAnswerUpperCase !in correctAnswer[i] ){
                println( "The correct answer is " + correctAnswer[i] + "\n" )
                result += "Incorrect"
            } else {
                println( "Correct!\n" )
                result += "Correct"
            }

            i += 1              //this is to increment the index of arrayQuestion

        } else {
            println( "Invalid Input\n" )
        }
    } while ( i < 10 )

    return result.groupingBy { it }.eachCount().filter { it.value >= 1 }    //will return the correct and incorrect key-value
}