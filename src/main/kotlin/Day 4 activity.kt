// val numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

fun main() {
    //create a loop that will print the following based on these conditions:
    //if the value is divisible by 3 = "Ping"
    //if the value is divisible by 5 = "Pong"
    //if the value is divible by 3 AND 5 = "PingPong"
    //else "x"
//    val numbers = listOf(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
//
//    numbers.forEach{
//        when {
//            (it % 3 == 0) && (it % 5 == 0) -> println("PingPong")
//            it % 3 == 0 -> println("Ping")
//            it % 5 == 0 -> println("Pong")
//            else -> println("x")
//        }
//    }

//    val subject1 = listOf(94, 82, 85)
//    val subject2 = listOf(83, 99, 97)
//    val subject3 = listOf(76, 89, 90)
//    val grades = listOf(subject1, subject2, subject3)
//    var x = 0
//
//    grades.forEach{ subject ->
//        val highestGradeSubject = subject.maxOrNull()
//        x += 1
//        println("Subject $x: $highestGradeSubject ")
//    }


    //finding perfect number
//    val numbers = (1..1000).toList()
//    var i = 1
//    var s = 0
//
//    val perfectNumbers = numbers.filter {
//        if () {
//            if ()
//        }
//    }
//    println("$perfectNumbers is a perfect number")

    val account: MutableMap<String, Any> = mutableMapOf<String, Any>(
        "username" to "Brandon",
        "balance" to 0
    )
    println(account)
    println(checkBalance(account))
//    deposit(account,500)
//    withdrawal(account,200)
    deposit(account, 5001)

}

//function to check whether a number is odd or even
fun checkBalance(account: Map<String, Any>): String {
    return "Your balance is ${account.getValue("balance")}"
}

fun deposit(account: MutableMap<String, Any>, amount: Int): Unit {
//    println(account.getValue("balance").javaClass)            //to check the type of the unit
//    println(amount.javaClass)

    if(account.getValue("balance").toString().toInt() > 5000 ) println("You cannot deposit more than 5000")
    else if((account.getValue("balance").toString().toInt() % 100 != 0) && account.getValue("balance").toString().toInt() % 100 > 0 ) println("You cannot deposit an amount not divisible by 100")
    else if(account.getValue("balance").toString().toInt() < 0 ) println("You cannot deposit less than 0")
    else {
        val newBalance = account.getValue("balance").toString().toInt() + amount
        account["balance"] = newBalance
        println("You just deposited: $amount. Your new balance is $newBalance")
    }
}

fun withdrawal(account: MutableMap<String, Any>, amount: Int): Unit {

    val newBalance = account.getValue("balance").toString().toInt() - amount
    account["balance"] = newBalance
    println("You just deposited: $amount. Your new balance is $newBalance")

//    if( amount <= account.getValue("balance")).toString().toInt()/2
}